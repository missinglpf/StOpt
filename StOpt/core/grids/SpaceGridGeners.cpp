#include "StOpt/core/grids/SpaceGridGeners.h"
// add include for all derived classes
#include "StOpt/core/grids/RegularLegendreGridGeners.h"
#include "StOpt/core/grids/GeneralSpaceGridGeners.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/core/grids/SparseSpaceGridBoundGeners.h"
#include "StOpt/core/grids/SparseSpaceGridNoBoundGeners.h"

// Register all wrappers
SerializationFactoryForSpaceGrid::SerializationFactoryForSpaceGrid()
{
    this->registerWrapper<RegularLegendreGridGeners>();
    this->registerWrapper<GeneralSpaceGridGeners>();
    this->registerWrapper<RegularSpaceGridGeners>();
    this->registerWrapper<SparseSpaceGridBoundGeners>();
    this->registerWrapper<SparseSpaceGridNoBoundGeners>();
}
